import {
  AccountRootPrincipal,
  Group,
  ManagedPolicy,
  Role,
} from "aws-cdk-lib/aws-iam";
import { Construct } from "constructs";
import { DenyUnlessMfaPolicy } from "./deny-unless-mfa-policy";

export class AdminGroup extends Construct {
  constructor(scope: Construct, id: string, path?: string) {
    super(scope, id);

    const adminGroup = new Group(this, "Resource", {
      path,
    });
    adminGroup.attachInlinePolicy(
      new DenyUnlessMfaPolicy(this, "DenyUnlessMfaPolicy")
    );

    const adminRole = new Role(this, "Role", {
      assumedBy: new AccountRootPrincipal().withConditions({
        Bool: { "aws:MultiFactorAuthPresent": "true" },
      }),
      managedPolicies: [
        ManagedPolicy.fromAwsManagedPolicyName("AdministratorAccess"),
      ],
      path,
    });
    adminRole.grantAssumeRole(adminGroup);
  }
}
