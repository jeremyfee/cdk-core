import {
  AccountRootPrincipal,
  Effect,
  Group,
  ManagedPolicy,
  PolicyStatement,
  Role,
} from "aws-cdk-lib/aws-iam";
import { Construct } from "constructs";
import { DenyUnlessMfaPolicy } from "./deny-unless-mfa-policy";
import { Stack } from "aws-cdk-lib";

export class DevelopGroup extends Construct {
  constructor(scope: Construct, id: string, path?: string) {
    super(scope, id);

    const developGroup = new Group(this, "Resource", {
      path,
    });
    developGroup.attachInlinePolicy(
      new DenyUnlessMfaPolicy(this, "DenyUnlessMfaPolicy")
    );

    const developRole = new Role(this, "Role", {
      assumedBy: new AccountRootPrincipal().withConditions({
        Bool: { "aws:MultiFactorAuthPresent": "true" },
      }),
      managedPolicies: [
        ManagedPolicy.fromAwsManagedPolicyName("ReadOnlyAccess"),
      ],
      path,
    });
    developRole.grantAssumeRole(developGroup);

    developRole.addToPrincipalPolicy(
      new PolicyStatement({
        sid: "AssumeCDKRoles",
        effect: Effect.ALLOW,
        actions: ["sts:AssumeRole"],
        resources: ["*"],
        conditions: {
          StringEquals: {
            "iam:ResourceTag/aws-cdk:bootstrap-role": [
              "image-publishing",
              "file-publishing",
              "deploy",
              "lookup",
            ],
          },
        },
      }),
    );

    developRole.addToPrincipalPolicy(
      new PolicyStatement({
        sid: "ReadCDKParameters",
        effect: Effect.ALLOW,
        actions: ["ssm:GetParameter"],
        resources: [
          Stack.of(this).formatArn({
            service: "ssm",
            resource: "parameter/cdk-bootstrap/*"
          })
        ]
      })
    );
  }
}
