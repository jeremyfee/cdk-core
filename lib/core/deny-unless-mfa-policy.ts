import { Policy, PolicyDocument } from "aws-cdk-lib/aws-iam";
import { Construct } from "constructs";

export class DenyUnlessMfaPolicy extends Policy {
  constructor(scope: Construct, id: string) {
    super(scope, id, {
      document: DENY_UNLESS_MFA_POLICY,
    });
  }
}

/**
 * Require MFA for all actions, except limited MFA configuration.
 * https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_examples_aws_my-sec-creds-self-manage.html
 *
 * NOTE: users with this policy cannot set password without mfa,
 * and should set initial password before policy is applied.
 */
export const DENY_UNLESS_MFA_POLICY = PolicyDocument.fromJson({
  Version: "2012-10-17",
  Statement: [
    {
      Sid: "AllowViewAccountInfo",
      Effect: "Allow",
      Action: ["iam:GetAccountPasswordPolicy", "iam:ListVirtualMFADevices"],
      Resource: "*",
    },
    {
      Sid: "AllowManageOwnPasswords",
      Effect: "Allow",
      Action: ["iam:ChangePassword", "iam:GetUser"],
      Resource: "arn:aws:iam::*:user/${aws:username}",
    },
    {
      Sid: "AllowManageOwnAccessKeys",
      Effect: "Allow",
      Action: [
        "iam:CreateAccessKey",
        "iam:DeleteAccessKey",
        "iam:ListAccessKeys",
        "iam:UpdateAccessKey",
        "iam:GetAccessKeyLastUsed",
      ],
      Resource: "arn:aws:iam::*:user/${aws:username}",
    },
    {
      Sid: "AllowManageOwnSigningCertificates",
      Effect: "Allow",
      Action: [
        "iam:DeleteSigningCertificate",
        "iam:ListSigningCertificates",
        "iam:UpdateSigningCertificate",
        "iam:UploadSigningCertificate",
      ],
      Resource: "arn:aws:iam::*:user/${aws:username}",
    },
    {
      Sid: "AllowManageOwnSSHPublicKeys",
      Effect: "Allow",
      Action: [
        "iam:DeleteSSHPublicKey",
        "iam:GetSSHPublicKey",
        "iam:ListSSHPublicKeys",
        "iam:UpdateSSHPublicKey",
        "iam:UploadSSHPublicKey",
      ],
      Resource: "arn:aws:iam::*:user/${aws:username}",
    },
    {
      Sid: "AllowManageOwnGitCredentials",
      Effect: "Allow",
      Action: [
        "iam:CreateServiceSpecificCredential",
        "iam:DeleteServiceSpecificCredential",
        "iam:ListServiceSpecificCredentials",
        "iam:ResetServiceSpecificCredential",
        "iam:UpdateServiceSpecificCredential",
      ],
      Resource: "arn:aws:iam::*:user/${aws:username}",
    },
    {
      Sid: "AllowManageOwnVirtualMFADevice",
      Effect: "Allow",
      Action: ["iam:CreateVirtualMFADevice"],
      Resource: "arn:aws:iam::*:mfa/*",
    },
    {
      Sid: "AllowManageOwnUserMFA",
      Effect: "Allow",
      Action: [
        "iam:DeactivateMFADevice",
        "iam:EnableMFADevice",
        "iam:ListMFADevices",
        "iam:ResyncMFADevice",
      ],
      Resource: "arn:aws:iam::*:user/${aws:username}",
    },
    {
      Sid: "DenyAllExceptListedIfNoMFA",
      Effect: "Deny",
      NotAction: [
        "iam:CreateVirtualMFADevice",
        "iam:EnableMFADevice",
        "iam:GetUser",
        "iam:GetMFADevice",
        "iam:ListMFADevices",
        "iam:ListVirtualMFADevices",
        "iam:ResyncMFADevice",
        "sts:GetSessionToken",
      ],
      Resource: "*",
      Condition: {
        BoolIfExists: {
          "aws:MultiFactorAuthPresent": "false",
        },
      },
    },
  ],
});
