import { Duration, aws_iam as iam, Stack, StackProps } from "aws-cdk-lib";
import { Construct } from "constructs";
import { AdminGroup } from "./core/admin-group";
import { DevelopGroup } from "./core/develop-group";
import { Bucket } from "aws-cdk-lib/aws-s3";

export class CdkCoreStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    new Bucket(this, "CoreBucket", {
      lifecycleRules: [
        {
          id: "deleteOldLogs",
          expiration: Duration.days(180),
          prefix: "AWSLogs/",
        },
      ],
    });

    this._createAdminGroup(props);

    new AdminGroup(this, "CoreAdminGroup", "/core/");
    new DevelopGroup(this, "CoreDevelopGroup", "/core/");
  }

  _createAdminGroup(props?: StackProps) {
    // admin role that requires MFA to assume
    const adminRole = new iam.Role(this, "MfaAdminRole", {
      assumedBy: new iam.AccountRootPrincipal().withConditions({
        Bool: { "aws:MultiFactorAuthPresent": "true" },
      }),
      managedPolicies: [
        iam.ManagedPolicy.fromAwsManagedPolicyName("AdministratorAccess"),
      ],
    });

    const group = new iam.Group(this, "MfaAdmin");
    // assume admin role
    group.addToPolicy(
      new iam.PolicyStatement({
        actions: ["sts:AssumeRole"],
        effect: iam.Effect.ALLOW,
        resources: [adminRole.roleArn],
      })
    );

    // Based on
    // https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_examples_aws_my-sec-creds-self-manage.html

    // NOTE: user must log in before adding to group,
    // and cannot set initial or reset password without mfa

    group.addToPolicy(
      new iam.PolicyStatement({
        sid: "mfaOptional",
        actions: [
          "iam:GetAccountPasswordPolicy",
          "iam:ListVirtualMFADevices",
          "sts:GetSessionToken",
        ],
        effect: iam.Effect.ALLOW,
        resources: ["*"],
      })
    );
    group.addToPolicy(
      new iam.PolicyStatement({
        sid: "mfaOptionalUser",
        actions: [
          // user
          "iam:GetUser",
          // mfa device
          "iam:CreateVirtualMFADevice",
          "iam:EnableMFADevice",
          "iam:ListMFADevices",
          "iam:ListVirtualMFADevices",
          "iam:ResyncMFADevice",
        ],
        effect: iam.Effect.ALLOW,
        resources: ["arn:aws:iam::*:user/${aws:username}"],
      })
    );

    group.addToPolicy(
      new iam.PolicyStatement({
        sid: "mfaRequired",
        actions: [
          // passwords
          "iam:ChangePassword",
          // access keys
          "iam:CreateAccessKey",
          "iam:DeleteAccessKey",
          "iam:ListAccessKeys",
          "iam:UpdateAccessKey",
          // mfa device
          "iam:DeleteVirtualMFADevice",
          "iam:DeactivateMFADevice",
          // signing certificate
          "iam:DeleteSigningCertificate",
          "iam:ListSigningCertificates",
          "iam:UpdateSigningCertificate",
          "iam:UploadSigningCertificate",
        ],
        conditions: {
          Bool: { "aws:MultiFactorAuthPresent": "true" },
        },
        effect: iam.Effect.ALLOW,
        resources: ["arn:aws:iam::*:user/${aws:username}"],
      })
    );
  }
}
