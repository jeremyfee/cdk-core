# CDK Core

This sets up the core environment for an aws account.

## Admin Group

Creates an IAM group with administrative privileges that requires MFA to log in.

> Users in this group are not allowed to set the initial or reset the password
> without MFA. You must create the user and have them log in to the console to
> reset the initial password before they are added to this group.

**Initial set up**

- Create access token with administrative privileges

- Bootstrap CDK environment

```
npm run cdk bootstrap aws://ACCOUNTID/REGION
```

- Deploy `core` stack

```
npm run cdk deploy core
```

- Deactivate access token with administrative privileges

**User set up**

- Create user (do not add to group yet).
- User must log in and reset password via console.
- Add user to group.
- User can now refresh `Security Credentials` page and set up MFA
- User logs out and logs in using MFA and can now create access token
- Configure AWS CLI:

`~/.aws/config`

```
[default]
output=json
region = us-east-1

[profile admin]
output = json
region = us-east-1
```

`~/.aws/credentials`

```
[default]
aws_access_key_id = ACCESS_KEY_ID
aws_secret_access_key = SECRET_ACCESS_KEY

[admin]
role_arn = ADMIN_ROLE_ARN
source_profile = default
mfa_serial = MFA_DEVICE_ARN
region = us-east-1
```

- User can now verify set up:

STS should report user credentials

```
aws sts get-caller-identity
```

User should be prompted for MFA code and STS should report admin role credentials

```
aws sts get-caller-identity --profile admin
```
